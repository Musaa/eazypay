package com.example.eazypay

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.eazypay.signin.SignInViewModel
import com.example.eazypay.signin.view.Mainview
import kotlinx.android.synthetic.main.activity_sign_in.*


class MainActivity : AppCompatActivity(), Mainview {
    override fun onLoginSuccess() {
        val intent = Intent(this, LoginActivity :: class.java)
        startActivity(intent)
    }

    override fun onLoginFail() {
        error("failed to log in")
    }

    private lateinit var viewModel: SignInViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = SignInViewModel(this)
        supportActionBar?.hide()

        val btnOPenActivity : Button = findViewById(R.id.btn_open_sign_in_activity)
        btnOPenActivity.setOnClickListener {viewModel.performLogin("iujiuji", "uyhuy" )

        }


    }
}










