package com.example.eazypay

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.eazypay.Account.ClientAccount
import com.example.eazypay.Payment.PaymentView
import com.example.eazypay.Payment.PaymentViewModel
import kotlinx.android.synthetic.main.activity_payment_details.*

class PaymentDetails : AppCompatActivity(), PaymentView {

    private lateinit var etCell: EditText
    private lateinit var etAmount: EditText
    private lateinit var btnreview: Button

    private lateinit var viewModel: PaymentViewModel
    private lateinit var myUserId: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_details)


        val bundle = intent.extras
        myUserId = bundle.getString("USER_ID")

        viewModel = PaymentViewModel(this)

        etCell = findViewById(R.id.etCell)
        etAmount = findViewById(R.id.etAmount)
        btnreview = findViewById(R.id.btnreview)

        //view.setOnClickListener { viewModel.getUserProfile(etCell.text.toString()) }

        btnreview.setOnClickListener {
            viewModel.getUserProfile(etCell.text.toString()) }

    }

    override fun onPaymentSuccess() {
        val text = "payment successful!"
        val duration = Toast.LENGTH_SHORT

        val toast = Toast.makeText(applicationContext, text, duration)
        toast.show()


    }

    override fun onPaymentFailed(errorMessage: String) {
        val text = "payment failed!"
        val duration = Toast.LENGTH_SHORT

        val toast = Toast.makeText(applicationContext, errorMessage, duration)
        toast.show()

    }

    override fun onGetUserSuccess(documentId: String) {

        viewModel.initiatePayment(etAmount.text.toString(),myUserId, documentId)


    }
}










