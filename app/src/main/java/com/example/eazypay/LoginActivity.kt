package com.example.eazypay

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import com.example.eazypay.profile.UserProfile
import com.example.eazypay.signin.LoginViewModel
import com.example.eazypay.signin.view.LoginView
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() , LoginView {
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var auth: FirebaseAuth

    private lateinit var etUsername: EditText
    private lateinit var etPassword: EditText
    private lateinit var btnSignIn: Button

    override fun onLoginSuccess(userID: String) {
        navigateToDashboard(userID)
    }

    override fun onLoginError(errorMessage: String) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layoutlogin)
        auth = FirebaseAuth.getInstance()
        loginViewModel = LoginViewModel(this, this, auth)

        etUsername = findViewById(R.id.username)
        etPassword = findViewById(R.id.password)

        btnSignIn = findViewById(R.id.login)

        btnSignIn.setOnClickListener { loginViewModel.performLogin(etUsername.text.toString(), etPassword.text.toString()) }

    }

    private fun navigateToDashboard(userId: String) {
        val intent = Intent(this, SignIn :: class.java)
        intent.putExtra("USER_ID", userId)
        startActivity(intent)
    }

}
