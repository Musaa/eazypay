package com.example.eazypay


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.fragment_pay.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class PayFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        val view = inflater.inflate(R.layout.fragment_pay, container, false)
        val btnpay : Button = view.findViewById(R.id.paybtn)

        val intentExtra = arguments
        val userId = intentExtra?.getString("USER_ID")

        Log.e("PayGrament", userId)


        val intent = Intent(activity, PaymentDetails::class.java)
        intent.putExtra("USER_ID", userId)
        btnpay.setOnClickListener { startActivity(intent) }
        // Inflate the layout for this fragment
        return view




    }


}
