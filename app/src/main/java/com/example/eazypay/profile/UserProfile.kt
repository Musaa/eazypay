package com.example.eazypay.profile

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.example.eazypay.R
import com.example.eazypay.repository.UserDetails
import com.google.firebase.firestore.FirebaseFirestore

class UserProfile : AppCompatActivity(),UserProfileView {
    override fun onGetProfileSuccess(userDetails: UserDetails) {
        populateView(userDetails)
    }

    override fun onGetProfileFail() {
            Toast.makeText(this, "There was an error", Toast.LENGTH_LONG).show()
    }

    private lateinit var tvName: TextView

    private lateinit var tvSurname: TextView

    private lateinit var tvCellNumber: TextView

    private lateinit var tvEmail: TextView

    private lateinit var viewModel: UserProfileViewModel
    private lateinit var firebaseDb: FirebaseFirestore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)
        val bundle  = intent.extras
        val userId = bundle.getString("USER_ID")

        tvName = findViewById(R.id.tvName)
        tvSurname = findViewById(R.id.tvSurname)
        tvCellNumber = findViewById(R.id.tvCellNumber)
        tvEmail = findViewById(R.id.tvEmail)
        firebaseDb = FirebaseFirestore.getInstance()

        viewModel = UserProfileViewModel(this, this, firebaseDb)
        viewModel.getUserProfile(userId)
    }
    private fun populateView(userDetails: UserDetails)
    {
        tvName.text = userDetails.name
        tvSurname.text = userDetails.surname
        tvCellNumber.text = userDetails.cellnumber
        tvEmail.text = userDetails.emailAddress

    }

    }






