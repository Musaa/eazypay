package com.example.eazypay.profile

import android.app.Activity
import com.example.eazypay.repository.UserDetails
import com.google.firebase.firestore.FirebaseFirestore

class UserProfileViewModel(private val activity: Activity, private val userProfileView: UserProfileView, private val firebaseDb: FirebaseFirestore) {

    fun getUserProfile(userId: String) {
    firebaseDb.collection("userdetails").document(userId).get().addOnSuccessListener {documentSnapshot ->
        val userDetails = UserDetails(userId, documentSnapshot["Name"] as String, documentSnapshot["Surname"] as String, documentSnapshot["EmailAddress"] as String, documentSnapshot["CellNumber"] as String)
        userProfileView.onGetProfileSuccess(userDetails)

    }

    }

    }
