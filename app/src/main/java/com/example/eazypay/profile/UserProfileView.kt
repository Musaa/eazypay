package com.example.eazypay.profile

import com.example.eazypay.repository.UserDetails

interface UserProfileView {
    fun onGetProfileSuccess(userDetails: UserDetails)
    fun onGetProfileFail()
}