package com.example.eazypay.Account

import android.app.Activity
import com.example.eazypay.repository.UserDetails
import com.google.firebase.firestore.FirebaseFirestore

class AccountViewModel (private val activity: Activity, private val userProfileView: AccountView, private val firebaseDb: FirebaseFirestore){
    fun getUserProfile(userId: String) {
        firebaseDb.collection("accounts").document(userId).get().addOnSuccessListener {documentSnapshot ->
            val clientAccount = ClientAccount(userId, documentSnapshot["AccountName"] as String, documentSnapshot["AccountNumber"] as String, documentSnapshot["CellNumber"] as String, documentSnapshot["available_balance"] as Number)
            userProfileView.onGetProfileSuccess(clientAccount)

        }

    }
}