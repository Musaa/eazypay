package com.example.eazypay.Account

import com.example.eazypay.repository.UserDetails

interface AccountView {
    fun onGetProfileSuccess(userDetails: ClientAccount)
    fun onGetProfileFail(errorMessage: String)
}