package com.example.eazypay

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_sign_in.*
import kotlin.contracts.Returns
import com.example.eazypay.HomeFragment as HomeFragment1


class SignIn : AppCompatActivity() {

    val manager = supportFragmentManager
    private lateinit var myUserId : String

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
        when (menuItem.itemId) {
            R.id.home -> {
                createHomeFragment()
                return@OnNavigationItemSelectedListener true
            }
            R.id.pay -> {
                createPayFragment()
                return@OnNavigationItemSelectedListener true
            }
            R.id.trans -> {
                createTransferFragment()
                return@OnNavigationItemSelectedListener true
            }
            R.id.buy -> {
                createBuyFragment()
                return@OnNavigationItemSelectedListener true
            }
            R.id.more -> {
                createMoreFragment()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)


        createHomeFragment()

        val bundle = intent.extras
        myUserId = bundle.getString("USER_ID")



        bottom_nav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

    }


    private fun createHomeFragment()
    {
        val transaction = manager.beginTransaction()
        val fragment = com.example.eazypay.HomeFragment()
        transaction.replace(R.id.fragmentholder,fragment)
        transaction.addToBackStack(null)
        transaction.commit()

    }

    private fun createPayFragment()
    {
        val transaction = manager.beginTransaction()
        val fragment = PayFragment()
        val bundle = Bundle ()
        bundle.putString("USER_ID", myUserId)
        fragment.arguments = bundle
        transaction.replace(R.id.fragmentholder,fragment)
        transaction.addToBackStack(null)
        transaction.commit()

    }
    private fun createTransferFragment()
    {
        val transaction = manager.beginTransaction()
        val fragment = TransferFragment()
        transaction.replace(R.id.fragmentholder,fragment)
        transaction.addToBackStack(null)
        transaction.commit()

    }
    private fun createBuyFragment()
    {
        val transaction = manager.beginTransaction()
        val fragment = BuyFragment()
        transaction.replace(R.id.fragmentholder,fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
    private fun createMoreFragment()
    {
        val transaction = manager.beginTransaction()
        val fragment = MoreFragment()
        transaction.replace(R.id.fragmentholder,fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}




