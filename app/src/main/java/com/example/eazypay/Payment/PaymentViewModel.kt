package com.example.eazypay.Payment

import android.util.Log
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import kotlin.math.log

class PaymentViewModel (private val view: PaymentView, private val firebaseDb: FirebaseFirestore = FirebaseFirestore.getInstance()) {

    fun initiatePayment(amount: String, myUuserId:String, recipientUserId: String) {

        val myAccount = firebaseDb.collection("accounts").document(myUuserId)
        val recipientAccount = firebaseDb.collection("accounts").document(recipientUserId)



        firebaseDb.runTransaction { transaction ->
            val snapShot: DocumentSnapshot = transaction.get(myAccount)
            val recipientSnapShot = transaction.get(recipientAccount)

            val myBalance = snapShot.getDouble("available_balance")
            val recipientBalance = recipientSnapShot.getDouble("available_balance")
            val paymentAmount = amount.toDouble()

            if (myBalance != null) {
                if (myBalance < paymentAmount) {
                    val errorMessage = "You have insufficient funds to make this transaction"
                    view.onPaymentFailed(errorMessage)
                    throw FirebaseFirestoreException(errorMessage,
                    FirebaseFirestoreException.Code.ABORTED)
                }
                if (recipientBalance == null){
                    val errorMessage = "Recipient doesn't have an account"
                    view.onPaymentFailed(errorMessage)
                    throw FirebaseFirestoreException(errorMessage,
                        FirebaseFirestoreException.Code.ABORTED)
                }
                val recipientNewBalance = recipientBalance + paymentAmount
                val myNewBalance = myBalance - paymentAmount

                transaction.update(myAccount, "available_balance", myNewBalance)
                transaction.update(recipientAccount, "available_balance", recipientNewBalance)
            }
        }.addOnSuccessListener { result ->
            view.onPaymentSuccess()

        }.addOnFailureListener { result ->
            view.onPaymentFailed(result.message.toString())
        }


    }

    fun getUserProfile(CellNumber: String){
        firebaseDb.collection("userdetails").whereEqualTo("CellNumber",CellNumber).get().addOnSuccessListener { documents ->
            for (document in documents) {
                val documentId = document.id
                Log.d( "userdetails", "userID")
                view.onGetUserSuccess(documentId    )


            }

        }.addOnFailureListener { error ->
            error.message?.let { view.onPaymentFailed(it) }
        }


    }


}




