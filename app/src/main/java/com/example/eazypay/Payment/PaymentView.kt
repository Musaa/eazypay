package com.example.eazypay.Payment

interface PaymentView {



    fun onPaymentSuccess()
    fun onPaymentFailed(errorMessage: String)
    fun onGetUserSuccess(documentId: String) {

    }

}