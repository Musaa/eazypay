package com.example.eazypay.signin.view

interface Mainview {
    fun onLoginSuccess()
    fun onLoginFail()
}