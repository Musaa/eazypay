package com.example.eazypay.signin.view

interface LoginView {
    fun onLoginSuccess(userID: String)

    fun onLoginError(errorMessage: String)
}