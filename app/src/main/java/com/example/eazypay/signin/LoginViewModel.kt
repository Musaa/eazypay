package com.example.eazypay.signin

import android.app.Activity
import com.example.eazypay.signin.view.LoginView
import com.google.firebase.auth.FirebaseAuth

class LoginViewModel(private val view: LoginView, private val activity: Activity, private val auth: FirebaseAuth) {

    fun performLogin(userName: String, passWord: String) {
        auth.signInWithEmailAndPassword(userName, passWord) .addOnCompleteListener(activity) {task -> if (task.isSuccessful) {
        view.onLoginSuccess(auth.currentUser?.uid.toString())
        } else {
            view.onLoginError("Login failed")
        }}
    }
}